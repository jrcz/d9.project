#!/bin/bash

PROGNAME=$(basename $0)
#echo "Program name is '$PROGNAME'."
CUR_DIR=`pwd`

# ------------------------------------------------------------------------------
# Validation
# ------------------------------------------------------------------------------

# EXTERNAL PROGRAM VALIDATION
composer -v > /dev/null 2>&1
COMPOSER=$?
#echo "Composer: $COMPOSER";
if [[ $COMPOSER -ne 0 ]]; then
    echo 'Composer is not installed.'
    exit 1
fi
git --version > /dev/null 2>&1
GIT=$?
#echo "Git: $GIT";
if [[ $GIT -ne 0 ]]; then
    echo 'Git is not installed.'
    exit 1
fi

# ARGUMENT VALIDATION
if [[ -z $1  ]]; then
  echo "Program '$PROGNAME' requires to provide a name of Drupal installation profile as the 1st argument." >&2
  exit 1
fi

if [[ -z $2 ]]; then
  echo "Program '$PROGNAME' requires to provide a language of Drupal installation (cs, en, etc.) as the 2nd argument." >&2
  exit 1
fi

if [[ ! -z $3 ]]; then
  echo "There are too many arguments to run the '$PROGNAME'. Please, provide only 2 arguments." >&2
  exit 1
fi

# ------------------------------------------------------------------------------
# Argument wizard
# ------------------------------------------------------------------------------

# 1st Argument
echo -n "Insert a database root username: "
read db_root
DB_ROOT=$(echo "$db_root")
echo "A database root username is: '$DB_ROOT'."

# 2nd Argument
echo -n "Insert a database root password: "
read db_root_pass
DB_ROOT_PASS=$(echo "$db_root_pass")
echo "A database root password is: '$DB_ROOT_PASS'."

# Database root credentials check
if ! mysql -u${DB_ROOT} -p${DB_ROOT_PASS} -e exit; then
  echo "Can't connect a database, please retry."
  exit 1
fi
#echo "$?" # return an exit code fo mysql command above

# 3rd Argument
USE_ROOT_CREDENTIALS=true;
while true; do
  echo -n "Do you want to use a root credentials within a project installation (Y/N)? (default=yes): "
  read use_db_root_credentials
  ((count++))
  # if an empty string
  if [[ -z $use_db_root_credentials ]]; then
    break
  fi
  valid_values=(yes YES Yes Y y)
  is_valid=false
  for i in "${valid_values[@]}"; do
    if [[ $i == $use_db_root_credentials ]]; then
      is_valid=true
      break
    fi
  done
  if [[ $is_valid == true ]]; then
    break
  fi
  invalid_values=(no NO No N n)
  is_invalid=false
  for j in "${invalid_values[@]}"; do
    if [[ $j == $use_db_root_credentials ]]; then
      is_invalid=true
      break
    fi
  done
  if [[ $is_invalid == true ]]; then
    echo "A database root credentials won't be used for a project database access."
    USE_ROOT_CREDENTIALS=false;
    break
  fi
done

if [[ $USE_ROOT_CREDENTIALS == false ]]; then
  # 3a Argument
  echo -n "Insert a project database user: "
  read db_usr
  echo "An inserted database username is: '$db_usr'."
  # Input is converted to lowercase, and cleaned up from special characters, and
  # whitespaces. Periods and slashes are converted to underscores.
  DB_USR=$(echo "$db_usr" | tr '[A-Z]' '[a-z]' | tr "'\"\`/{}()[]!,;:<>?@#$%\^&*=+|" " " | tr -d " " | tr ".-" "_")
  echo "A sanitized database username is: '$DB_USR'."
fi

# 4th Argument
echo -n "Insert a project database name: "
read db_name
echo "An inserted database name is: '$db_name'."
# Input is converted to lowercase, and cleaned up from special characters, and
# whitespaces. Periods and slashes are converted to underscores.
DB_NAME=$(echo "$db_name" | tr '[A-Z]' '[a-z]' | tr "'\"\`/{}()[]!,;:<>?@#$%\^&*=+|" " " | tr -d " " | tr ".-" "_")
echo "A sanitized database name is: '$DB_NAME'."

# 5th Argument
echo -n "Insert a project site name: "
read site_name
SITE_NAME=$(echo "$site_name")
echo "A project site name is: '$SITE_NAME'."

# 6th Argument
echo -n "Insert a project domain: "
read domain
echo "An inserted project domain is: '$domain'."
# Input is converted to lowercase, and cleaned up from special characters, and
# whitespaces. Periods and slashes are converted to underscores.
DOMAIN=$(echo "$domain" | tr '[A-Z]' '[a-z]' | tr "'\"\`/{}()[]!,;:<>?@#$%\^&*=+|" " " | tr -d " " | tr "_" "-")
echo "A sanitized project domain is: '$DOMAIN'."

# ------------------------------------------------------------------------------
# Running...
# ------------------------------------------------------------------------------

# Db
# TODO: předělat - této konstrukci nerozumím
DB_EXISTS=$(MYSQL_PWD="${DB_ROOT_PASS}" mysql \
 -u "${DB_ROOT}" \
 --skip-column-names \
 --batch \
 -e "SHOW DATABASES LIKE '${DB_NAME}'" | wc -l)

REMOVE_MYSQL_WARNING="mysql: [Warning] Using a password on the command line interface can be insecure."
if [[ $DB_EXISTS == 1 ]]; then
  mysql -u${DB_ROOT} -p${DB_ROOT_PASS} -e "DROP DATABASE $DB_NAME;" 2>/dev/null | grep -v "$REMOVE_MYSQL_WARNING"
fi
mysql -u${DB_ROOT} -p${DB_ROOT_PASS} -e "CREATE DATABASE $DB_NAME CHARACTER SET utf8 COLLATE utf8_general_ci" 2>/dev/null | grep -v "$REMOVE_MYSQL_WARNING"

if [[ $USE_ROOT_CREDENTIALS == false ]]; then
  # Database password generation
  DB_PASS=$(date +%s|sha256sum|base64|head -c 12)
  echo "Generated database password is: $DB_PASS"
  mysql -u${DB_ROOT} -p${DB_ROOT_PASS} -e "CREATE USER $DB_USR@localhost IDENTIFIED BY '$DB_NAME';" 2>/dev/null | grep -v "$REMOVE_MYSQL_WARNING"
  mysql -u${DB_ROOT} -p${DB_ROOT_PASS} -e "GRANT ALL PRIVILEGES ON $DB_NAME.* TO '$DB_USR'@'localhost';" 2>/dev/null | grep -v "$REMOVE_MYSQL_WARNING"
  mysql -u${DB_ROOT} -p${DB_ROOT_PASS} -e "FLUSH PRIVILEGES;" 2>/dev/null | grep -v "$REMOVE_MYSQL_WARNING"
else
  DB_USR=$DB_ROOT
  DB_PASS=$DB_ROOT_PASS
fi

# Development modules
composer require --dev drupal/devel
composer require --dev kint-php/kint
composer require --dev drupal/coffee

# Contrib modules
composer require drupal/admin_toolbar
composer require drupal/environment_indicator
composer require drupal/focal_point
composer require drupal/allowed_formats
#composer require drupal/linkit - TODO: not available for d9 yet
composer require drupal/ckeditorheight
composer require drupal/maxlength
composer require drupal/pathauto
composer require drupal/redirect
composer require drupal/token
composer require drupal/diff
composer require drupal/twig_tweak
composer require drupal/asset_injector
composer require drupal/inline_entity_form
composer require drupal/config_split

# Private Git repos
git clone git@jrcz.bitbucket.org:jrcz/gridpuls.profile.git web/profiles/gridpuls_profile
git clone git@jrcz.bitbucket.org:jrcz/gridpuls.git web/modules/custom/gridpuls
git clone git@jrcz.bitbucket.org:jrcz/gridpuls.admin.git web/themes/custom/gridpuls_admin
git clone git@jrcz.bitbucket.org:jrcz/gridpuls.front.git web/themes/custom/gridpuls_front
echo "" >> .gitignore
echo "" >> .gitignore
echo "/web/profiles/gridpuls_profile" >> .gitignore
echo "/web/modules/custom/gridpuls" >> .gitignore
echo "/web/themes/custom/gridpuls_admin" >> .gitignore
echo "/web/themes/custom/gridpuls_front" >> .gitignore

# Drupal Install
drush site:install $1 --db-url=mysql://$DB_USR:$DB_PASS@localhost:80/$DB_NAME --locale=$2 --site-name=$SITE_NAME

# Adjust permissions before actions in filesystem
sudo chown -R $USER:$USER ${CUR_DIR}/web/sites/
sudo chmod 775 ${CUR_DIR}/web/sites/default/
sudo chmod -R 777 ${CUR_DIR}/web/sites/default/

# services.yml:
cp ${CUR_DIR}/web/sites/default/default.services.yml ${CUR_DIR}/web/sites/default/services.yml
sed -i 's/debug: false/debug: true/' ${CUR_DIR}/web/sites/default/services.yml

# settings.php
sed -i 's/# if (file_exists($app_root/if (file_exists($app_root/' ${CUR_DIR}/web/sites/default/settings.php
sed -i 's/#   include $app_root/  include $app_root/' ${CUR_DIR}/web/sites/default/settings.php
sed -i 's/# }/}/' ${CUR_DIR}/web/sites/default/settings.php
echo "\$settings['trusted_host_patterns'] = ['^${DOMAIN}$'];" >> ${CUR_DIR}/web/sites/default/settings.php
echo "\$config['environment_indicator.indicator']['bg_color'] = '#4caf50';" >> ${CUR_DIR}/web/sites/default/settings.php
echo "\$config['environment_indicator.indicator']['fg_color'] = '#000000';" >> ${CUR_DIR}/web/sites/default/settings.php
echo "\$config['environment_indicator.indicator']['name'] = 'local';" >> ${CUR_DIR}/web/sites/default/settings.php

# settings.local.php
cp ${CUR_DIR}/web/sites/example.settings.local.php ${CUR_DIR}/web/sites/default/settings.local.php
echo "\$config['system.site']['name'] = 'd9 LOCAL';" >> ${CUR_DIR}/web/sites/default/settings.local.php
echo "\$config['dblog.settings']['row_limit'] = 1000000;" >> ${CUR_DIR}/web/sites/default/settings.local.php
echo "\$config['system.logging']['error_level'] = 'verbose';" >> ${CUR_DIR}/web/sites/default/settings.local.php

sudo chmod 755 -R ${CUR_DIR}/web/sites/
sudo chown -R $USER:$USER ${CUR_DIR}/web/sites/default/files/
sudo chmod -R 777 ${CUR_DIR}/web/sites/default/files/
sudo chmod 744 ${CUR_DIR}/web/sites/default/settings.php

drush user:password admin admin
drush cr